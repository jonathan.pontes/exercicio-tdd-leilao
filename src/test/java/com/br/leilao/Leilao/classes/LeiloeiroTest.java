package com.br.leilao.Leilao.classes;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTest {

    Leiloeiro leiloeiro;

    @Test
    public void deveRetornarOMaiorLance() {
        leiloeiro = new Leiloeiro();

        List<Lance> lances = new ArrayList<>();
        Usuario usuario = new Usuario(1, "Joao");
        Lance lance = new Lance(usuario, 10.0);

        Usuario usuarioDois = new Usuario(1, "Maria");
        Lance lanceDois = new Lance(usuarioDois, 22.0);

        Usuario usuarioTres = new Usuario(2, "Overchico");
        Lance lanceTres = new Lance(usuarioTres, 57.0);

        lances.add(lance);
        lances.add(lanceDois);
        lances.add(lanceTres);

        Leilao leilao = new Leilao();
        leilao.adicionarNovoLance(lance);
        leilao.adicionarNovoLance(lanceDois);
        leilao.adicionarNovoLance(lanceTres);

        Lance maiorLance = leiloeiro.retornarMaiorLance(leilao);
        Assertions.assertEquals(57.0, maiorLance.getValorDoLance());
        Assertions.assertEquals("Overchico", maiorLance.getUsuario().getName());

        //TODO Faz sentido essas variaveis?
        Assertions.assertEquals("Overchico", leiloeiro.nome);
        Assertions.assertEquals(3, leiloeiro.leilao.lances.size());
    }

    @Test
    public void deveLancarExceptionParaLeilaoSemLance() {
        leiloeiro = new Leiloeiro();
        Leilao leilao = new Leilao();

        Assertions.assertThrows(RuntimeException.class, () -> leiloeiro.retornarMaiorLance(leilao));
    }

}
