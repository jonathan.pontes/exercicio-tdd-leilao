package com.br.leilao.Leilao.classes;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LeilaoTest {

    Leilao leilao;

    @Test
    public void deveAdicionarNovoLanceNaListaVazia() {
        leilao = new Leilao();
        Usuario usuario = new Usuario(1, "Joao");
        Lance lance = new Lance(usuario, 10.0);

        Lance lanceAdicionado = leilao.adicionarNovoLance(lance);

        Assertions.assertEquals(1, lanceAdicionado.getUsuario().getId());
        Assertions.assertEquals("Joao", lanceAdicionado.getUsuario().getName());
        Assertions.assertEquals(10.0, lanceAdicionado.getValorDoLance());
    }

    @Test
    public void naoPodeAceitarLanceMenorQueOLanceJaAdicionado() {
        leilao = new Leilao();

        Usuario usuario = new Usuario(1, "Maria");
        Lance lanceUm = new Lance(usuario, 50.0);

        Usuario usuarioDois = new Usuario(2, "Overchico");
        Lance lanceDois = new Lance(usuarioDois, 13.0);

        leilao.adicionarNovoLance(lanceUm);

        Assertions.assertThrows(RuntimeException.class, () -> leilao.adicionarNovoLance(lanceDois));
    }

    @Test
    public void deveAdicionarNovoLanceNaListaPopulada() {
        leilao = new Leilao();

        Usuario usuario = new Usuario(1, "Dollynho");
        Lance lanceUm = new Lance(usuario, 5.0);

        Usuario usuarioDois = new Usuario(2, "Overchico");
        Lance lanceDois = new Lance(usuarioDois, 50.0);

        leilao.adicionarNovoLance(lanceUm);
        leilao.adicionarNovoLance(lanceDois);

        Assertions.assertEquals(2, leilao.lances.size());
        Assertions.assertEquals(2, leilao.lances.get(1).getUsuario().getId());
        Assertions.assertEquals("Overchico", leilao.lances.get(1).getUsuario().getName());
        Assertions.assertEquals(50.0, leilao.lances.get(1).getValorDoLance());
    }

}
