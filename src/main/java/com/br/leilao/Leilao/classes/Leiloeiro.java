package com.br.leilao.Leilao.classes;

public class Leiloeiro {
    //TODO Qual necessidade dessas variaveis
    public String nome;
    public Leilao leilao;

    public Lance retornarMaiorLance(Leilao leilaoCadastral) {
        Lance maiorLance = new Lance();

        if (!leilaoCadastral.lances.isEmpty()) {
            //TODO existe a necessidade
            leilao = leilaoCadastral;
            maiorLance = leilaoCadastral.lances.get(0);
            for (Lance lance : leilaoCadastral.lances) {
                if (lance.getValorDoLance() > maiorLance.getValorDoLance()) {
                    maiorLance = lance;
                    nome = lance.getUsuario().getName();
                }
            }
            return maiorLance;
        } else {
            throw new RuntimeException("Favor informar um leilão valido. O leilão informado não possui lances!");
        }
    }
}
