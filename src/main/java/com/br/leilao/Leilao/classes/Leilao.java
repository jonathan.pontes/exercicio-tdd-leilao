package com.br.leilao.Leilao.classes;

import java.util.ArrayList;
import java.util.List;

public class Leilao {
    public List<Lance> lances = new ArrayList<>();

    public Lance adicionarNovoLance(Lance lance) {
        lances.add(validarLance(lance));
        return lances.get(indiceUltimoLance());
    }

    private Lance validarLance(Lance lance) {
        if (!lances.isEmpty()) {
            Lance ultimoLance = lances.get(indiceUltimoLance());
            if (lance.getValorDoLance() > ultimoLance.getValorDoLance()) {
                return lance;
            } else {
                throw new RuntimeException("O Valor do ultimo lance é: " + ultimoLance + " /n. Favor adicionar um lance maior que o ultimo");
            }
        }
        return lance;
    }

    private int indiceUltimoLance() {
        int quantidadeDeLances = lances.size();
        return (quantidadeDeLances - 1);
    }
}
